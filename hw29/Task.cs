﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace hw29
{
    public delegate void EventHandler();
    public class Task
    {
        public string Name { get; set; }
        public string Status { get; set; }
        public int Priority { get; set; }
        public DateTime Date { get; set; }
        public DateTime DeadLine { get; set; }
        [JsonIgnore]
        public IState State { get; set; }


        public event EventHandler myEvent;

        public string ShowInJson()
        {
            return JsonConvert.SerializeObject(this);
        }

        public Task()
        {
        }


        public override string ToString()
        {
            return $"Name: {Name}\nSatatus: {Status}\nPriority: {Priority}\nDate: {Date}\nDeadLine: {DeadLine}";
        }
        public void InvokeEvent()
        {
            myEvent.Invoke();
        }


        public void SetState(List<Task> list, Task task)
        {

            if (Status == "new")
            {
                Console.WriteLine("You can change your task status to In_progress or delete it.");
                string reply = Console.ReadLine();
                if (reply == "change")
                {
                    Status = "in_progress";
                    State = new In_progress1();
                }
                else if (reply == "del")
                {
                    list.Remove(task);
                }

            }
            if (Status == "in_progress")
            {
                Console.WriteLine("You can change your task status to Done or delete it.");
                string reply = Console.ReadLine();
                if (reply == "change")
                {
                    Status = "done";
                    State = new Done1();
                }
                else if (reply == "del")
                {
                    list.Remove(task);
                }

            }
            if (Status == "done")
            {
                Console.WriteLine("You can delete it.");
                string reply = Console.ReadLine();
                if (reply == "change")
                {
                    Status = "done";
                    State = new Done1();
                }
                else if (reply == "del")
                {
                    list.Remove(task);
                }

            }

        }


        public class NameComparer : IComparer<Task>
        {
            public int Compare(Task x, Task y)
            {
                if (x != null && y != null)
                {
                    return x.Name.CompareTo(y.Name);
                }

                return 0;
            }
        }


         public class StatusComparer : IComparer<Task>
         {
                public int Compare(Task x, Task y)
                {
                    if (x != null && y != null)
                    {
                        return x.Status.CompareTo(y.Status);

                    }

                    return 0;
                }
         }
        public class DateComparer : IComparer<Task>
        {
            public int Compare(Task x, Task y)
            {
                if (x != null && y != null)
                {
                    return x.Date.CompareTo(y.Date);
                }

                return 0;
            }
        }
        public class PriorityComparer : IComparer<Task>
        {
            public int Compare(Task x, Task y)
            {
                if (x != null && y != null)
                {
                    return x.Priority.CompareTo(y.Priority);
                }

                return 0;
            }
        }




    }
}
