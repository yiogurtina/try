﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static hw29.Task;

namespace hw29
{

    class Program

    {
        public enum Operation
        {
            Name = 1,
            Status = 2,
            Priority = 3,
            Data = 4
        }
        delegate bool SortingHandler(Task task1, Task task2);
        static void Main(string[] args)
        {

            Task task1 = new Task()
            {
                Name = "DoSmth",
                Status = "new",
                Priority = 2,
                Date = DateTime.Now.AddDays(7),
                DeadLine = DateTime.Now.AddDays(97)
            };
            Task task2 = new Task()
            {
                Name = "GoSmwh",
                Status = "in_progress",
                Priority = 1,
                Date = DateTime.Now.AddDays(-10),
                DeadLine = DateTime.Now.AddDays(80)
            };
            Task task3 = new Task()
            {
                Name = "GiveSomeone",
                Status = "done",
                Priority = 5,
                Date = DateTime.Now.AddDays(5),
                DeadLine = DateTime.Now.AddDays(95)
            };
            Task task4 = new Task()
            {
                Name = "SnatchSmth",
                Status = "in_progress",
                Priority = 4,
                Date = DateTime.Now.AddDays(-5),
                DeadLine = DateTime.Now.AddDays(97)
            };

            List<Task> list = new List<Task>();
            list.Add(task1);
            list.Add(task2);
            list.Add(task3);
            list.Add(task4);
            DataLoader.Save(list);
            DateComparer data = new DateComparer();
            list.Sort(data);
            
            foreach (var x in list)
            {
                Console.WriteLine(x);
            }
            

            Console.Write("List of action\n");
            Console.Write("1 : Add New Task\n2 : Change status or delete it\n3 : Set Deadline\n4 : Missed Deadline notification\n5 : Sorting");
            int num = Convert.ToInt32(Console.ReadLine());
            switch (num)
            {
                case 1:
                    Console.WriteLine("Enter new task: 1.name 2.priority");

                    Task task = new Task()
                    {
                        Name = Console.ReadLine(),
                        Status = "new",
                        Priority = Convert.ToInt32(Console.ReadLine()),
                        Date = DateTime.Now.AddDays(7),


                    };
                    task.DeadLine = task.Date.AddMonths(3);
                    list.Add(task);
                    DataLoader.Save(list);
                    foreach (var x in list)
                    {
                        Console.WriteLine(x);
                    }
                    break;
                case 2:
                    Task task11 = new Task();
                    task11.SetState(list, task11);
                    
                    break;
                case 3:
                    
                    break;
                case 5:
                    SortList(list);
                    break;

                default:
                    Console.WriteLine("Error. You entered invalid number");
                    break;
            }
            Console.ReadKey();
        }

        private static void CreateNewTask1(Task task)
        {
            task.Name = Console.ReadLine();
            task.Status = "new";
            task.Priority = Convert.ToInt32(Console.ReadLine());
            task.Date = DateTime.Now.AddDays(7);
        }

        
        public static void SortList(List<Task> list)
        {
           
            Console.Write("How would you like to sort your list?\n");
            Console.Write("1 : Name\n2 : Status\n3 : Priority\n4 : Data\n");
            int num = Convert.ToInt32(Console.ReadLine());
            switch (num)
            {
                case 1:
                    NameComparer name = new NameComparer();
                    list.Sort(name);
                    break;
                case 2:
                    StatusComparer status = new StatusComparer();
                    list.Sort(status);
                    break;
                case 3:
                    PriorityComparer priority = new PriorityComparer();
                    list.Sort(priority);
                    break;

                default:
                    Console.WriteLine("Error. You entered invalid number");
                    SortList(list);
                    break;
            }

            foreach (var x in list)
            {
                Console.WriteLine(x);
            }
            SortList(list);           


        }

        class ArraySort
        {
            public static void Sort<T>(List<T> sortArray, SortingHandler handler) where T : Task
            {
                bool mySort = true;
                do
                {
                    mySort = false;
                    for (int i = 0; i < sortArray.Count - 1; i++)
                    {
                        if (handler(sortArray[i + 1], sortArray[i]))
                        {
                            T j = sortArray[i];
                            sortArray[i] = sortArray[i + 1];
                            sortArray[i + 1] = j;
                            mySort = true;
                        }
                    }
                } while (mySort);
            }

        }
    }
}
