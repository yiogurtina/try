﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hw29
{
    public class DataLoader
    {
        const string directory = "../../Data";
        const string fileName = "mytasks.json";

        public static void Save(List<Task> tasks)
        {
            string json = JsonConvert.SerializeObject(tasks);
            SaveFile(json);
        }

        public static List<Task> Load()
        {
            {
                string content = File.ReadAllText($"{directory}/{fileName}");
                if (string.IsNullOrEmpty(content))
                {
                    throw new ApplicationException($"файл {fileName} пустой");
                }
                List<Task> tasks = JsonConvert.DeserializeObject<List<Task>>(content);
                //foreach (Task task in tasks)
                //{
                //    task.SetState();
                //}

                return tasks;
            }
            //catch (DirectoryNotFoundException)
            //{
            //    Console.WriteLine($"Отсутствует директория {directory}");
            //    return new Task[0];
            //}
            //catch (FileNotFoundException)
            //{
            //    Console.WriteLine($"Отсутствует файл {fileName}");
            //    return new Task[0];
            //}
            //catch (ApplicationException ex)
            //{
            //    Console.WriteLine(ex.Message);
            //    return new Task[0];
            //}
        }

        private static void SaveFile(string content)
        {
            try
            {
                File.WriteAllText($"{directory}/{fileName}", content);
            }
            catch (DirectoryNotFoundException)
            {
                Directory.CreateDirectory(directory);
                SaveFile(content);
            }
        }
    }

}
