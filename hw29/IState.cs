﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hw29
{
    public interface IState
    {
        
        void In_progress(Task task);
        void Done(Task task);
    }
    public class New : IState

    {        

        public void In_progress(Task task)
        {
            task.Status = "in_progress";
            task.State = new In_progress1();
        }

        public void Done(Task task)
        {
            task.Status = "done";
            task.State = new Done1();
        }


    }
    public class In_progress1 : IState

    {
        public void In_progress(Task task)
        {
            Console.WriteLine("You can not change state to in_progress");
        }

        public void Done(Task task)
        {
            task.Status = "done";
            task.State = new Done1();
        }


    }
    public class Done1 : IState

    {
        public void In_progress(Task task)
        {
            Console.WriteLine("You can not change state to in_progress");
        }

        public void Done(Task task)
        {
            Console.WriteLine("You can not change state to done");
        }


    }
}
